from flask import Flask, request, jsonify, Response
import json, datetime
from settings import *
from book_model import *
import jwt
from user_model import User
from functools import wraps

app.config['SECRET_KEY'] = 'meow'

def valid_book_object(book_object):
    if ("name" in book_object 
            and "price" in book_object 
            and "isbn" in book_object):
        return True
    else:
        return False

def token_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        token = request.args.get('token')
        try:
            jwt.decode(token, app.config['SECRET_KEY'])
            return f(*args, **kwargs)
        except:
            return jsonify({'error': 'Token invalid.'}), 401
    return wrapper
#
# GETs
#
################################################################################
@app.route('/')
def hello_world():
    return 'Totes the home page!'

# GET /books?<token>
@app.route('/books')
def get_books():
    return jsonify({'books': Book.get_all_books()})

# GET /books/<isbn>
    # 2nd parameter passed in url will be stored as 'isbn'
    #   and can then be used as a parameter for the method
@app.route('/books/<int:isbn>')
def get_book_by_isbn(isbn):
    return_value = Book.get_book(isbn)
    return jsonify(return_value)


@app.route('/query-example')
def query_example():
    language = request.args.get('language') #if key doesn't exist, returns None
    l = request.get_json()

    return '''<h1>The language value is: {}</h1>'''.format(l)

#
# POSTs
#
################################################################################
# POST /books
# {
#     "name": "Book Name",
#     "price": 6.99,
#     "isbn": 0123456789
# }
@app.route('/books', methods=['POST'])
@token_required
def add_books():
    req_data = request.get_json()

    # sanitize data
    if(valid_book_object(req_data)):
        Book.add_book(req_data['name'], req_data['price'], req_data['isbn'])        
        # construct response
        response = Response("", status=201, mimetype='application/json')
        response.headers['Location'] = "/books/" + str(req_data['isbn'])

        return response
    else:
        invalid_book_object_error_message = {
            "error": "Invalid book passed in the request",
            "help_string": "Data passed in should be formatted like this: {...things...}"
        }
        # construct response
            # json.dumps() converts our dictionary into json
        response = Response(json.dumps(invalid_book_object_error_message), status=400, mimetype='application/json')
        return response

@app.route('/json-example', methods=['POST']) #GET requests will be blocked
def json_example():
    req_data = request.get_json()

    language = req_data['language']
    framework = req_data['framework']
    python_version = req_data['version_info']['python'] #two keys are needed because of the nested object
    example = req_data['examples'][0] #an index is needed because of the array
    boolean_test = req_data['boolean_test']

    return '''
           The language value is: {}
           The framework value is: {}
           The Python version is: {}
           The item at index 0 in the example list is: {}
           The boolean value is: {}'''.format(language, framework, python_version, example, boolean_test)

# POST /login
@app.route('/login', methods=['POST'])
def get_token():
    req_data = request.get_json()
    username = str(req_data['username'])
    password = str(req_data['password'])
    
    match = User.username_password_match(username, password)

    if match:
        expiration_date = datetime.datetime.utcnow() + datetime.timedelta(seconds=100)
        token = jwt.encode({'exp': expiration_date}, app.config['SECRET_KEY'], algorithm='HS256')
        return token
    else:
        return Response('', 401, mimetype='application/json')

#
# PUTs
#
################################################################################
# PUT /books/0123456789
# {
#     "name": "The Odyssey",
#     "price": 19.99
# }
@app.route('/books/<int:isbn>', methods=['PUT'])
@token_required
def replace_book(isbn):
    req_data = request.get_json()
    Book.replace_book(req_data['isbn'], req_data['name'], req_data['price'])
    response = Response("", status=204)

    return response


#
# PATCHs
#
################################################################################
@app.route('/books/<int:isbn>', methods=['PATCH'])
@token_required
def update_book(isbn):
    req_data = request.get_json()
    updated_book = {}
    if('name' in req_data):
        Book.update_book_name(isbn, req_data['price'])
    # TODO: fix this section. Sending a PATCH with price returns a 500
    if('price' in req_data):
        Book.update_book_name(isbn, req_data['name'])
    
    response = Response("", status=204)
    response.headers['Location'] = "/books/" + str(isbn)
    return response


#
# DELETEs
#
################################################################################
# DELETE /books/0123456789

@app.route('/books/<int:isbn>', methods=['DELETE'])
@token_required
def delete_book(isbn):
    if(Book.delete_book(isbn)):
        response = Response("", status=204)
        return response
    else:
        response = Response("Something when wrong.", status=404)
        return response  

# start server
app.run(debug=True, port=5000)